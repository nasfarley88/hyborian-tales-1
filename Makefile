ALL=main.pdf

all: $(ALL)

main.pdf: main.tex beyond_black_river.tex gods_north.tex jewels_gwahlur.tex people_black_circle.tex red_nails.tex shadows_zamboula.tex devil_iron.tex hour_dragon.tex hyborian_age.tex queen_black_coast.tex shadows_moonlight.tex witch_born.tex
	pdflatex $<
	pdflatex $<

read:
	evince main.pdf &
clean:
	rm -rf *.aux *.log *.out *.pdf *.lof *.lot *.toc *.cut
