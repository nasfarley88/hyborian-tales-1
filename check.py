# silly script to check for missed unicode characters

import sys

for n in sys.argv[1:]:
    print n

    with open(n) as f:
        l = 1
        s = f.readline()
        while len(s) > 0:
            for c in s:
                if ord(c) > 127:
                    print "Oops", l
            l += 1
            s = f.readline()
