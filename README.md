# Hyborian Tales

A Collection of Classic Sword & Sorcery Fiction
by Robert E. Howard

## Background

I wanted to read some Howard stories and looked
around online. Much to my surprise I found that
Project Gutenberg had transcribed quite a few
stories that had passed into the public domain.
Total win!

But reading Howard in plain text or ugly HTML
just didn't seem right: I wanted to read these
stories in a "properly typeset" format instead.
Years of experience with LaTeX meant I knew a
way to get exactly that, so I started converting
the plain text files to LaTeX and designed the
basic layout for a "serious" book.

Eventually it dawned on me that others might also
enjoy a properly typeset collection of Howard's
brand of Sword & Sorcery, so I started thinking
in terms of a "definite public domain edition"
instead of just a temporary crutch until I was
done reading. So I also started collecting the
available art from the original publications and
putting it back into the text.

## Goals

Here are the current goals for the project:

* Produce a properly typeset edition of all
  Robert E. Howard Sword & Sorcery stories
  available in the public domain.
* Keep to a classic layout/design, don't go
  overboard with graphical/ornamental elements.
* Include the original artwork for the stories
  as far as possible.
* Do only the minimal editing required for
  proper typesetting, do not rewrite Howard
  in any way.
* Target primarily the 6in x 9in hardcover
  format supported by lulu.com as POD.
* Support other layouts as far as possible
  without sacrificing the primary 6in x 9in
  layout; layouts suitable for a variety of
  tablets would be nice.

I am sure these will evolve, but it's roughly
what I'd like to stick to at this time.

## Internals

Here are some "rules" for the LaTeX internals:

* Avoid unicode in the source files, stick to
  traditional LaTeX commands for producing
  the "exotic" characters required by some of
  the stories.
* Define commands to take care of certain
  repetitive layout issues (we're off the mark
  a little on this right now).
* Use the appropriate dashes and quote marks
  throughout.

There's probably more but it'll evolve over
time.

## Copyright Concerns

I have not yet decided on how to handle concerns
over "compilation copyright" and related issues.
As far as I understand the Project Gutenberg
license I have fulfilled their terms by not using
their name to advertise but simply crediting them.
Same with The Internet Archive and Wikimedia
Commons. I'll put together detailed credits for
the final release, right now I have credits for
some things but not others. My own work on this
(compilating, editing, layout) will be covered by
some form of Creative Commons license, details to
be decided. Contributors should be okay with this
as well, please speak up if you have concerns.
